#!/bin/bash
set -e
## It will add new openid application

if [[ -z $APP_CALLBACKS ]] || [[ -z $APP_ID ]] || [[ -z $APP_SECRET ]]
then
    echo "You should add environment variables:"
    echo "  docker exec -e APP_ID=<app name> -e APP_SECRET=<app secret> -e APP_CALLBACKS=<app callback url wiht https://...> hydra bash -c '/scripts/add_app.sh'"
    echo "Please run it again setting environment variables"
    exit 1
fi

hydra clients create \
--endpoint http://hydra:4445/ \
--id $APP_ID \
--secret $APP_SECRET \
--grant-types client_credentials,authorization_code,refresh_token \
--token-endpoint-auth-method client_secret_post \
--response-types code \
--scope openid,offline,profile,email \
--callbacks $APP_CALLBACKS

hydra token client \
--endpoint http://hydra:4444/ \
--client-id $APP_ID \
--client-secret $APP_SECRET
